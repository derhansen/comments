<?php
namespace Derhansen\Comments\Tests\Acceptance;

class CommentsCest
{
    /**
     * @param \AcceptanceTester $I
     */
    public function rootPageWorks(\AcceptanceTester $I)
    {
        $I->amOnPage('/');
        $I->see('HELLO WORLD!');
    }

    /**
     * @param \AcceptanceTester $I
     */
    public function commentsListViewShowExpectedComments(\AcceptanceTester $I)
    {
        $I->wantTo('Check if listview shows 2 predefined comments');
        $I->amOnPage('/index.php?id=2');
        $I->see('This is my first comment!');
        $I->see('This is my second comment!');
    }

    /**
     * @param \AcceptanceTester $I
     */
    public function commentsFormShowExpectedValidationErrorsWithNoBlacklist(\AcceptanceTester $I)
    {
        $I->wantTo('Check if comment form shows validation errors');
        $I->amOnPage('/index.php?id=3');
        $I->waitForElement('#comment-name');

        $I->wantTo('Submit form without any data');
        $I->click('#comment-submit');
        $I->seeElement('.form-item__comment-name .field-errors .error');
        $I->seeElement('.form-item__comment-email .field-errors .error');
        $I->seeElement('.form-item__comment-comment .field-errors .error');

        $I->wantTo('Submit form without email and comment');
        $I->fillField('#comment-name', 'Torben Hansen');
        $I->click('#comment-submit');
        $I->dontSeeElement('.form-item__comment-name .field-errors .error');
        $I->seeElement('.form-item__comment-email .field-errors .error');
        $I->seeElement('.form-item__comment-comment .field-errors .error');

        $I->wantTo('Submit form with invalid email and without comment');
        $I->fillField('#comment-name', 'Torben Hansen');
        $I->fillField('#comment-email', 'invalid-email');
        $I->click('#comment-submit');
        $I->dontSeeElement('.form-item__comment-name .field-errors .error');
        $I->seeElement('.form-item__comment-email .field-errors .error');
        $I->seeElement('.form-item__comment-comment .field-errors .error');

        $I->wantTo('Submit form without comment');
        $I->fillField('#comment-name', 'Torben Hansen');
        $I->fillField('#comment-email', 'torben@derhansen.com');
        $I->click('#comment-submit');
        $I->dontSeeElement('.form-item__comment-name .field-errors .error');
        $I->dontSeeElement('.form-item__comment-email .field-errors .error');
        $I->seeElement('.form-item__comment-comment .field-errors .error');
    }

    /**
     * @param \AcceptanceTester $I
     */
    public function commentsFormSavesValidCommentWithNoBlacklist(\AcceptanceTester $I)
    {
        $timestamp = time();

        $I->wantTo('Check if comment form saves valid comment');
        $I->amOnPage('/index.php?id=3');
        $I->waitForElement('#comment-name');

        $I->wantTo('Submit form with valid data and see the new comment');
        $I->fillField('#comment-name', 'Torben Hansen');
        $I->fillField('#comment-email', 'torben@derhansen.com');
        $I->fillField('#comment-comment', 'This is a test comment - time: ' . $timestamp);
        $I->click('#comment-submit');
        $I->canSee('Torben Hansen', '.comment-item__name .comment-item__value');
        $I->canSee('torben@derhansen.com', '.comment-item__email .comment-item__value');
        $I->canSee('This is a test comment - time: ' . $timestamp, '.comment-item__comment .comment-item__value');
    }

    /**
     * @param \AcceptanceTester $I
     */
    public function commentsFormDoesNotAcceptBlacklistedWords(\AcceptanceTester $I)
    {
        $I->wantTo('Check if comment form shows validation error for blacklisted words');
        $I->amOnPage('/index.php?id=4');
        $I->waitForElement('#comment-name');

        $I->fillField('#comment-name', 'Torben Hansen');
        $I->fillField('#comment-email', 'torben@derhansen.com');
        $I->fillField('#comment-comment', 'This is a test1 comment');
        $I->click('#comment-submit');
        $I->seeElement('.form-item__comment-comment .field-errors .error');

        $I->fillField('#comment-name', 'Torben Hansen');
        $I->fillField('#comment-email', 'torben@derhansen.com');
        $I->fillField('#comment-comment', 'This is a test2 comment');
        $I->click('#comment-submit');
        $I->seeElement('.form-item__comment-comment .field-errors .error');

        $I->fillField('#comment-name', 'Torben Hansen');
        $I->fillField('#comment-email', 'torben@derhansen.com');
        $I->fillField('#comment-comment', 'test1, this is a test comment');
        $I->click('#comment-submit');
        $I->seeElement('.form-item__comment-comment .field-errors .error');
    }
}
