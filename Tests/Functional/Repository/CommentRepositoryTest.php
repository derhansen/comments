<?php
namespace Derhansen\Comments\Tests\Functional\Repository;

use Derhansen\Comments\Domain\Model\Dto\CommentDemand;
use Derhansen\Comments\Domain\Repository\CommentRepository;
use Nimut\TestingFramework\TestCase\FunctionalTestCase;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Object\ObjectManager;

/**
 * Functional Test case.
 *
 * @author Torben Hansen <torben@derhansen.com>
 */
class CommentRespositoryTest extends FunctionalTestCase
{
    /** @var \TYPO3\CMS\Extbase\Object\ObjectManagerInterface */
    protected $objectManager;

    /** @var  CommentRepository */
    protected $commentRepository;

    /** @var array */
    protected $testExtensionsToLoad = ['typo3conf/ext/comments'];

    /**
     * Setup
     *
     * @throws \TYPO3\CMS\Core\Tests\Exception
     * @return void
     */
    public function setUp()
    {
        parent::setUp();
        $this->objectManager = GeneralUtility::makeInstance(ObjectManager::class);
        $this->commentRepository = $this->objectManager->get(CommentRepository::class);
        $this->importDataSet(__DIR__ . '/../Fixtures/tx_comments_domain_model_comment.xml');
    }

    /**
     * Test if all expected records for the functional testsuite are returned
     *
     * @test
     * @return void
     */
    public function findAllReturnsExpectedAmountOfRecords()
    {
        $comments = $this->commentRepository->findAll();
        $this->assertSame(5, $comments->count());
    }

    /**
     * @test
     * @return void
     */
    public function findDemandedByNameReturnsExpectedResult()
    {
        /** @var CommentDemand $commentDemand */
        $commentDemand = $this->objectManager->get(CommentDemand::class);
        $commentDemand->setName('Brian J. North');
        $result = $this->commentRepository->findDemanded($commentDemand);
        $this->assertSame(1, $result->count());
    }

    /**
     * @test
     * @return void
     */
    public function findDemandedByEmailReturnsExpectedResult()
    {
        /** @var CommentDemand $commentDemand */
        $commentDemand = $this->objectManager->get(CommentDemand::class);
        $commentDemand->setEmail('abw@email.domain');
        $result = $this->commentRepository->findDemanded($commentDemand);
        $this->assertSame(2, $result->count());
    }

    /**
     * @test
     * @return void
     */
    public function findDemandedByPageUidReturnsExpectedResult()
    {
        /** @var CommentDemand $commentDemand */
        $commentDemand = $this->objectManager->get(CommentDemand::class);
        $commentDemand->setPageUid(2);
        $result = $this->commentRepository->findDemanded($commentDemand);
        $this->assertSame(2, $result->count());
    }

    /**
     * @test
     * @return void
     */
    public function findDemandedByPageUidAndEmailReturnsExpectedResult()
    {
        /** @var CommentDemand $commentDemand */
        $commentDemand = $this->objectManager->get(CommentDemand::class);
        $commentDemand->setEmail('abw@email.domain');
        $commentDemand->setPageUid(2);
        $result = $this->commentRepository->findDemanded($commentDemand);
        $this->assertSame(1, $result->count());
    }
}
