<?php
namespace Derhansen\Comments\Tests\Unit\Controller;

use Nimut\TestingFramework\TestCase\UnitTestCase;

/**
 * Test case.
 *
 * @author Torben Hansen <torben@derhansen.com>
 */
class CommentControllerTest extends UnitTestCase
{
    /**
     * @var \Derhansen\Comments\Controller\CommentController
     */
    protected $subject = null;

    protected function setUp()
    {
        parent::setUp();
        $this->subject = $this->getMockBuilder(\Derhansen\Comments\Controller\CommentController::class)
            ->setMethods(['redirect', 'forward', 'addFlashMessage'])
            ->disableOriginalConstructor()
            ->getMock();
    }

    protected function tearDown()
    {
        parent::tearDown();
    }

    /**
     * @test
     */
    public function listActionFetchesAllCommentsFromRepositoryAndAssignsThemToView()
    {
        $allComments = $this->getMockBuilder(\TYPO3\CMS\Extbase\Persistence\ObjectStorage::class)
            ->disableOriginalConstructor()
            ->getMock();

        $commentRepository = $this->getMockBuilder(\Derhansen\Comments\Domain\Repository\CommentRepository::class)
            ->setMethods(['findAll'])
            ->disableOriginalConstructor()
            ->getMock();
        $commentRepository->expects(self::once())->method('findAll')->will(self::returnValue($allComments));
        $this->inject($this->subject, 'commentRepository', $commentRepository);

        $view = $this->getMockBuilder(\TYPO3\CMS\Extbase\Mvc\View\ViewInterface::class)->getMock();
        $view->expects(self::once())->method('assign')->with('comments', $allComments);
        $this->inject($this->subject, 'view', $view);

        $this->subject->listAction();
    }
}
