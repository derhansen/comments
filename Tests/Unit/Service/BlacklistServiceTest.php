<?php
namespace Derhansen\Comments\Tests\Unit\Service;

use Derhansen\Comments\Service\BlacklistService;
use Nimut\TestingFramework\TestCase\UnitTestCase;
use TYPO3\CMS\Extbase\Configuration\ConfigurationManager;

/**
 * Test case.
 *
 * @author Torben Hansen <torben@derhansen.com>
 */
class EmailServiceTest extends UnitTestCase
{

    /**
     * @var \Derhansen\Comments\Service\BlacklistService
     */
    protected $subject = null;

    /**
     * Setup
     *
     * @return void
     */
    protected function setUp()
    {
        $this->subject = new BlacklistService();
    }

    /**
     * Teardown
     *
     * @return void
     */
    protected function tearDown()
    {
        unset($this->subject);
    }

    /**
     * @return array
     */
    public function validCommentDataProvider()
    {
        return [
            'noBlacklistDefined' => [
                'This is a test comment',
                [],
                true
            ],
            'oneWordMatchWithExactCase' => [
                'This is a test comment with a Blacklisted word',
                ['blacklist' => 'Blacklisted'],
                false
            ],
            'multipleWordMatchCaseInsensitive' => [
                'This is a test comment with a Blacklisted word, test 123',
                ['blacklist' => 'blacklisted, test'],
                false
            ],
            'blacklistedWordAtbeginningOfString' => [
                'This is a test comment with a Blacklisted word, test 123',
                ['blacklist' => 'this'],
                false
            ]
        ];
    }

    /**
     * @test
     * @dataProvider validCommentDataProvider
     */
    public function validCommentReturnsExpectedResults($comment, $settings, $expected)
    {
        $mockConfigurationManager = $this->getMock(ConfigurationManager::class, ['getConfiguration'], [], '', false);
        $mockConfigurationManager->expects($this->once())->method('getConfiguration')->will(
            $this->returnValue($settings)
        );
        $this->inject($this->subject, 'configurationManager', $mockConfigurationManager);

        $result = $this->subject->validComment($comment);
        $this->assertSame($expected, $result);
    }
}
