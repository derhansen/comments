<?php
defined('TYPO3_MODE') or die();

/**
 * Plugins
 */
\TYPO3\CMS\Extbase\Utility\ExtensionUtility::registerPlugin(
    'comments',
    'Picomments',
    'The comments plugin'
);

/**
 * Remove unused fields
 */
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_excludelist']['comments_picomments'] = 'layout,recursive,select_key';

/**
 * Add Flexform for comments plugin
 */
$GLOBALS['TCA']['tt_content']['types']['list']['subtypes_addlist']['comments_picomments'] = 'pi_flexform';
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addPiFlexFormValue(
    'comments_picomments',
    'FILE:EXT:comments/Configuration/FlexForms/Picomments.xml'
);

/**
 * Default TypoScript
 */
\TYPO3\CMS\Core\Utility\ExtensionManagementUtility::addStaticFile(
    'comments',
    'Configuration/TypoScript',
    'Comments Demo Extension'
);
