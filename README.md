Comments - A Demo extension for TYPO3 CI
========================================

Warning: This TYPO3 extension is for demonstration purposes only. Do **NOT** use in production!

Requirements to run local acceptance tests
------------------------------------------

* Selenium Server Standalone (run with `java -jar <selenium-filename>`)  
* TYPO3 website based on TYPO3 8.7 with imported SQL dump `Build/Database/dump.sql` 
* TYPO3 website must be available on `http://comments-ci.typo3.local/`
* `127.0.0.1 selenium__standalone-chrome` in your local hostfile

Installation
------------
* Clone the extension
* `composer install`

Unit tests
----------
```
.Build/bin/phpunit --colors -c .Build/vendor/nimut/testing-framework/res/Configuration/UnitTests.xml Tests/Unit/
```

Functional tests
----------------
```
export typo3DatabaseName="typo3";
export typo3DatabaseHost="mysql";
export typo3DatabaseUsername="root";
export typo3DatabasePassword="password";

.Build/bin/phpunit --colors -c .Build/vendor/nimut/testing-framework/res/Configuration/FunctionalTests.xml Tests/Functional
```

Acceptance Tests
----------------
```
.Build/bin/codecept run Acceptance -c Build/AcceptanceTests.yml
```

Requirements to run tests on GitLab CI
--------------------------------------

* Local docker build image

*Dockerfile*

```
FROM ubuntu:xenial

ENV DEBIAN_FRONTEND=noninteractive

# Install basics
RUN apt-get update
RUN apt-get install -y curl git

# Install MySQL
RUN apt-get install -y mysql-server

# Install PHP 7.0
RUN apt-get install -y php7.0 php7.0-mysql php7.0-mcrypt php7.0-mbstring php7.0-cli php7.0-gd php7.0-curl php7.0-xml php7.0-soap php7.0-zip

VOLUME /root/composer

# Environmental Variables
ENV COMPOSER_HOME /root/composer

# Install Composer
RUN curl -sS https://getcomposer.org/installer | php -- --install-dir=/usr/local/bin --filename=composer && \
        composer selfupdate

# Goto temporary directory.
WORKDIR /tmp

RUN php --version
RUN composer --version

CMD ["./run.sh"]

```

* Build Docker Container with `docker build -t derhansen/typo3-ci:latest .`
* Register new GitLab CI Runner (which is using the new Docker Container) as described here http://docs.gitlab.com/runner/register/
* Register new runner with tags used in .gitlab-ci.yml
* Add `pull_policy = "if-not-present"` in `/etc/gitlab-runner/config.toml` config for the new runner (will tell GitLab CI to look up for Docker image locally)

 