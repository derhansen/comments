<?php
defined('TYPO3_MODE') || die('Access denied.');

\TYPO3\CMS\Extbase\Utility\ExtensionUtility::configurePlugin(
    'Derhansen.' . $_EXTKEY,
    'Picomments',
    [
        'Comment' => 'list,create',
    ],
    [
        'Comment' => 'create',
    ]
);
