<?php

namespace Derhansen\Comments\Domain\Repository;

/***
 *
 * This file is part of the "Comments - CI Demo" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Torben Hansen <torben@derhansen.com>
 *
 ***/

use Derhansen\Comments\Domain\Model\Dto\CommentDemand;

/**
 * The repository for Comments
 */
class CommentRepository extends \TYPO3\CMS\Extbase\Persistence\Repository
{

    /**
     * Returns the objects of this repository matching the given demand
     *
     * @param CommentDemand $demand
     * @return array|\TYPO3\CMS\Extbase\Persistence\QueryResultInterface
     */
    public function findDemanded(CommentDemand $demand)
    {
        $constraints = [];
        $query = $this->createQuery();

        if ($demand->getName() !== '') {
            $constraints[] = $query->equals('name', $demand->getName());
        }

        if ($demand->getEmail() !== '') {
            $constraints[] = $query->equals('email', $demand->getEmail());
        }

        if ($demand->getPageUid() > 0) {
            $constraints[] = $query->equals('page_uid', $demand->getPageUid());
        }

        if (count($constraints) > 0) {
            $query->matching($query->logicalAnd($constraints));
        }

        return $query->execute();
    }
}
