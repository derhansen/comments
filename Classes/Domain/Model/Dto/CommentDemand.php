<?php

namespace Derhansen\Comments\Domain\Model\Dto;

/***
 *
 * This file is part of the "Comments - CI Demo" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Torben Hansen <torben@derhansen.com>
 *
 ***/

/**
 * Comment demand
 */
class CommentDemand extends \TYPO3\CMS\Extbase\DomainObject\AbstractEntity
{
    /**
     * @var string
     */
    protected $name = '';

    /**
     * @var string
     */
    protected $email = '';

    /**
     * @var int
     */
    protected $pageUid = 0;

    /**
     * @return string
     */
    public function getName() : string
    {
        return $this->name;
    }

    /**
     * @param string $name
     */
    public function setName(string $name)
    {
        $this->name = $name;
    }

    /**
     * @return string
     */
    public function getEmail() : string
    {
        return $this->email;
    }

    /**
     * @param string $email
     */
    public function setEmail(string $email)
    {
        $this->email = $email;
    }

    /**
     * @return int
     */
    public function getPageUid() : int
    {
        return $this->pageUid;
    }

    /**
     * @param int $pageUid
     */
    public function setPageUid(int $pageUid)
    {
        $this->pageUid = $pageUid;
    }
}
