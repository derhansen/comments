<?php
namespace Derhansen\Comments\Service;

/***
 *
 * This file is part of the "Comments - CI Demo" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Torben Hansen <torben@derhansen.com>
 *
 ***/

use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * BlacklistService
 */
class BlacklistService
{
    /**
     * @var \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface
     */
    protected $configurationManager;

    /**
     * Injects the Configuration Manager and loads the settings
     *
     * @param \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager
     */
    public function injectConfigurationManager(
        \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface $configurationManager
    ) {
        $this->configurationManager = $configurationManager;
    }

    /**
     * @param $comment
     * @return bool
     */
    public function validComment($comment)
    {
        $valid = true;

        $settings = $this->configurationManager->getConfiguration(
            \TYPO3\CMS\Extbase\Configuration\ConfigurationManagerInterface::CONFIGURATION_TYPE_SETTINGS,
            'Comments',
            'Picomments'
        );

        $wordBlacklist = GeneralUtility::trimExplode(',', $settings['blacklist']);
        foreach ($wordBlacklist as $blacklistedWord) {
            if (stripos($comment, $blacklistedWord) !== false) {
                $valid = false;
                break;
            }
        }
        return $valid;
    }
}
