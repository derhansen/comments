<?php

namespace Derhansen\Comments\Controller;

/***
 *
 * This file is part of the "Comments - CI Demo" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Torben Hansen <torben@derhansen.com>
 *
 ***/

use Derhansen\Comments\Domain\Model\Comment;

/**
 * CommentController
 */
class CommentController extends \TYPO3\CMS\Extbase\Mvc\Controller\ActionController
{
    /**
     * commentRepository
     *
     * @var \Derhansen\Comments\Domain\Repository\CommentRepository
     * @inject
     */
    protected $commentRepository = null;

    /**
     * action list
     *
     * @return void
     */
    public function listAction()
    {
        $comments = $this->commentRepository->findAll();
        $this->view->assign('comments', $comments);
    }

    /**
     * create action
     *
     * @param Comment $comment
     */
    public function createAction(Comment $comment)
    {
        $comment->setDate(new \DateTime());
        $comment->setPageUid($GLOBALS['TSFE']->id);
        $this->commentRepository->add($comment);
        $uriBuilder = $this->uriBuilder;
        $uri = $uriBuilder->setTargetPageUid($GLOBALS['TSFE']->id)->build();
        $this->redirectToUri($uri);
    }
}
