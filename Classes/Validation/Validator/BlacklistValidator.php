<?php

namespace Derhansen\Comments\Validation\Validator;

/***
 *
 * This file is part of the "Comments - CI Demo" Extension for TYPO3 CMS.
 *
 * For the full copyright and license information, please read the
 * LICENSE.txt file that was distributed with this source code.
 *
 *  (c) 2017 Torben Hansen <torben@derhansen.com>
 *
 ***/

use Derhansen\Comments\Service\BlacklistService;

/**
 * BlacklistValidator
 */
class BlacklistValidator extends \TYPO3\CMS\Extbase\Validation\Validator\AbstractValidator
{
    /**
     * @var BlacklistService
     */
    protected $blacklistService = null;

    /**
     * @param BlacklistService $blacklistService
     */
    public function injectBlacklistService(\Derhansen\Comments\Service\BlacklistService $blacklistService)
    {
        $this->blacklistService = $blacklistService;
    }

    /**
     * @param string $value
     * @return bool
     */
    protected function isValid($value)
    {
        if (!$this->blacklistService->validComment($value)) {
            $this->addError('The comment contains a blacklisted word', 1262366779);
            return false;
        } else {
            return true;
        }
    }
}
